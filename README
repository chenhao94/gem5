#Modified gem5 simulator

## Configuration and Running of Our Simulator

We modified gem5 simulator for our research purpose. In this simulator, we can 
configure the latencies for different cache lines. We use `configs/example/se.py` 
as the gem5 configuration/running script, and we can change the configuration by
running options (use `run.sh --help` for more informations), and/or the following config files:

* cpus: `src/cpu/simple/TimingSimpleCPU.py` (we use `TimingSimpleCPU` aka `timing`)
* caches: `configs/common/Caches.py`
* cache line: `icache.config` and `dcache.config` for detailed latencies, `numSets` lines and `assoc` numbers per line, each number represent the latency for one perticular cache line(-1 means disabled, detailed configuration only for set associative policy)
* And you can also modified the python files under `src` for `Param` of each class

We use `make.sh` or `make_debug.sh` to build our simulator and use `run.sh -c <benchmark and the args>` to run.
You can also use `doxygen` to generate the documents of the codes by `cd src && doxygen Doxyfile`

###SPEC 2006

This repository does not contain SPEC benchmarks, you should install them by yourself. 
However, we provide the scripts for automatically running them. 
You can specify the location of your benchmarks in `configs/example/Mybench.py`, 
then type `./run_spec2006.sh` to run. The outputs and the statistics are in `m5out/spec2006/`.

## Structure of the Codes

This simulator combined C++ and Python together by Swig. So there are a lot of interactions between C++ classes and Python classes.
E.g., the `Param` class for each class in C++ is actually configured by the python sources located in the same directory.

### Caches

There are a lot of classes for simulating caches. `BaseCache` and `Cache` is for basic information of a cache (size, latency, statistics, etc.).
`CacheBlk` stores actual data, it represents the cache lines. `Tag`s are for organizing the cache lines and the replacing strategies. `CacheSet`
is for the sets in a set-associative cache.

####Latency

In original version, there are latencies set for `Cache`, we keep that in case potential usage, but we rename them as `base_` latencies.
For fine-grained latency configuration, we define the latency functions to replace the uniform latencies. The latency of each block 
will be intialized as the default value given by `configs/common/Caches.py`, and then set to user-defined value. 
Therefore, if you want to use uniform access latency set in `configs/common/Caches.py`, you can simply remove the content
of `setBlkLatency` in `src/mem/cache/my_utils.cc`. (base_set_assoc  and lru only)

####Disable Cachelines

When you set a `-1` for a cacheline, we will regard it as disabled. We check it in `CacheSet::findBlk` and `BaseTags::findVictim` to prevent them
from being used. You cannot set all cachelines in one set disabled. It will lead to an undefined behavior. (base_set_assoc and lru only)

## Document of Original gem5

This is the gem5 simulator.

The main website can be found at http://www.gem5.org

A good starting point is http://www.gem5.org/Introduction, and for
more information about building the simulator and getting started
please see http://www.gem5.org/Documentation and
http://www.gem5.org/Tutorials.

To build gem5, you will need the following software: g++ or clang,
Python (gem5 links in the Python interpreter), SCons, SWIG, zlib, m4,
and lastly protobuf if you want trace capture and playback
support. Please see http://www.gem5.org/Dependencies for more details
concerning the minimum versions of the aforementioned tools.

Once you have all dependencies resolved, type 'scons
build/<ARCH>/gem5.opt' where ARCH is one of ALPHA, ARM, NULL, MIPS,
POWER, SPARC, or X86. This will build an optimized version of the gem5
binary (gem5.opt) for the the specified architecture. See
http://www.gem5.org/Build_System for more details and options.

With the simulator built, have a look at
http://www.gem5.org/Running_gem5 for more information on how to use
gem5.

The basic source release includes these subdirectories:

* configs: example simulation configuration scripts
* ext: less-common external packages needed to build gem5
* src: source code of the gem5 simulator
* system: source for some optional system software for simulated systems
* tests: regression tests
* util: useful utility programs and files

To run full-system simulations, you will need compiled system firmware
(console and PALcode for Alpha), kernel binaries and one or more disk
images. Please see the gem5 download page for these items at
http://www.gem5.org/Download

If you have questions, please send mail to gem5-users@gem5.org

Enjoy using gem5 and please share your modifications and extensions.
