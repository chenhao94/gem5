#include <string>
#include <fstream>
#include "mem/cache/my_utils.hh"

void initBlkLatency(CacheBlk *blk, unsigned int num, unsigned int hit_latency, unsigned int response_latency)
{
    for (int i = 0; i < num; ++i)
    {
        blk[i].blk_accessLatency = Cycles(hit_latency);

        blk[i].blk_fillLatency = Cycles(response_latency);
    }
}

void setBlkLatency(CacheBlk *blk, unsigned int numSets, unsigned int assoc, L1CacheType type)
{
    using namespace std;
    string filename;
    if (type == DCACHE)
    {
        cerr << "set data cache block latency here!" << endl;
        filename = "dcache.config";
    }
    else if (type == ICACHE)
    {
        cerr << "set instruction cache block latency here!" << endl;
        filename = "icache.config";
    }
    else
        return;
    cerr << ((type==DCACHE)?"dcache":"icache") << " sets and ways: " << numSets << " * " << assoc << endl;
    ifstream input(filename);
    if (!input.is_open())
    {
        cerr << "ERROR: " << filename << ": file does not exist!!" << endl;
        exit(0);
    }

    for (int i = 0; i < numSets; ++i)
        for (int j = 0; j < assoc; ++j)
        {
            int x;
            input >> x;
            blk[i * assoc + j].blk_accessLatency =
                 blk[i * assoc + j].blk_fillLatency = Cycles(x);
        }

    input.close();
}
