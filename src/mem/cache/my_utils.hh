#ifndef __MY_UTILS_HH__
#define __MY_UTILS_HH__
#include <iostream>
#include "mem/cache/blk.hh"

enum L1CacheType {ICACHE, DCACHE, OTHERS};

void initBlkLatency(CacheBlk *blk, unsigned int num, unsigned int hit_latency, unsigned int response_latency);

void setBlkLatency(CacheBlk *blk, unsigned int numSets, unsigned int assoc, L1CacheType type);

#endif
