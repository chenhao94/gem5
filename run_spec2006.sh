#!/bin/bash
source ./run_config.sh
CONFIG_SCRIPT=./configs/example/cmp.py
OPTIONS=$OPTIONS" --cacheline_size=64 --l1d_assoc=2 --l1i_assoc=2"
echo $OPTIONS
for bench in "gobmk" "libquantum" "xalancbmk" "soplex" "calculix" "specrand_f" "bzip2" "povray" "hmmer" "omnetpp";do
    outdir=m5out/spec2006/$bench
    mkdir -p $outdir
    $BIN.fast -d $outdir -r -e $CONFIG_SCRIPT $OPTIONS -b $bench --output=$outdir/output.txt --errout=$outdir/errout.txt & 
done
